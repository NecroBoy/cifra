import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"],
})
@Injectable()
export class ProfileComponent {
  id = 0;
  status: any;
  person: any;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    route.queryParams.subscribe((params) => {
      this.id = params["id"];
    });

    http
      .get<any>("https://reqres.in/api/users/" + this.id)
      .subscribe((data) => {
        this.person = data["data"];
        console.log(this.person);
      });
  }

  onEmailChange(event: any) {
    this.person.email = event.target.value;
  }

  onFirstChange(event: any) {
    this.person.first_name = event.target.value;
  }

  onLastChange(event: any) {
    this.person.last_name = event.target.value;
  }

  editUser() {
    // console.log(this.person);

    this.http
      .put<any>("https://reqres.in/api/users/" + this.id, this.person)
      .subscribe({
        next: (data) => {
          console.log(data);

          this.status = "Обновлено успешно";

          setTimeout(() => {
            this.status = "";
          }, 2000);
        },
        error: (error) => {
          console.error("Ошибка с сервера", error);

          this.status = "Ошибка на стороне сервера";

          setTimeout(() => {
            this.status = "";
          }, 2000);
        },
      });
  }
}
