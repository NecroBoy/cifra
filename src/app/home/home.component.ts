import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
@Injectable()
export class HomeComponent {
  persons = [];
  resources = [];

  constructor(private http: HttpClient) {
    http.get<any>("https://reqres.in/api/users?page=2").subscribe((data) => {
      this.persons = data["data"];
      console.log(this.persons);
    });

    http.get<any>("https://reqres.in/api/unknown").subscribe((data) => {
      this.resources = data["data"];
      console.log(this.resources);
    });
  }

  deleteUser(id: any) {
    // console.log(id)
    this.http.delete<any>("https://reqres.in/api/users/" + id).subscribe({
      next: (data) => {
        console.log(`ID ${id} удалено с сервера и из DOM успешно`);
        this.persons = this.persons.filter((item) => item["id"] !== id);
      },
      error: (error) => {
        console.error("Ошибка с сервера", error);
      },
    });
  }
}
